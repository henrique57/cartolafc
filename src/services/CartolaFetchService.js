import { Alert } from 'react-native';

const uri = 'https://api.cartolafc.globo.com/partidas';

export default class CartolaFetchService{

    static get() {
        return fetch(uri).then(resp => resp.json());
    }

}