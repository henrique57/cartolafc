import React from 'react';
import { createStackNavigator } from 'react-navigation';

import * as Views from './views';

export const Router = () => {
    const StackNavigator = createStackNavigator(
        {...Views},
        {
            navigationOptions: {
                headerStyle: {
                    backgroundColor: '#e3511b'
                },
                headerTitleStyle: {
                    color: '#ffffff',
                }
            },
        }
    );

    return <StackNavigator />;
};