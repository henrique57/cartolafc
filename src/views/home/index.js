import HomeScreen from './HomeScreen';
import { HomeScreenNavigationOptions } from './HomeScreenNavigationOptions';

export const HOME = {
    screen: HomeScreen,
    navigationOptions: HomeScreenNavigationOptions,
};