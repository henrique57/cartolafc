import { StyleSheet } from 'react-native';

export const STYLES = StyleSheet.create({
    tela: {
        //backgroundColor: '#f0595a',
        alignItems: 'center',        
    },
    logo: {
        margin: 30,
        borderRadius: 120,
        height: 250,
        width: 250,
    },
    container: {
        borderRadius: 5,
        width:300,
        backgroundColor: "#e27109",
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
    },
    texto:{
        color: '#a05108',
        margin: 10,
        alignItems: 'center',
        flex: 2
    },
    botao:{
        
        alignItems: 'center',
        flex:1,
        marginBottom: 10,
        width:10, 
        height:10
    },
});