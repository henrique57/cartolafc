import React, {Component} from 'react';
import { Text, View, Button, Image } from 'react-native';
import { STYLES } from './styles';

export default class HomeScreen extends Component{

    render(){
        return(
            <View style={STYLES.tela}>

                <View >
                <Image style={STYLES.logo} 
                    source={{uri: "https://www.torcedores.com/content/uploads/2016/09/cartola-fc-1.jpg"}}/>  
                </View>
                <View style={STYLES.container}>
                    <Button style={STYLES.botao}
                        onPress={() => this.props.navigation.navigate('POSICAO')}
                        title="Ranking dos Clubes"
                        color="#fff"
                        />                    
                </View>
                <View style={STYLES.container}>
                    <Button style={STYLES.botao}
                        onPress={() => this.props.navigation.navigate('PARTIDA')}
                        title="Próximas Partidas"
                        color="#fff"
                        />                    
                </View>
            </View>
        );
    }

}