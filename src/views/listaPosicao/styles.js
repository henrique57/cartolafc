import { StyleSheet } from 'react-native';

export const STYLES = StyleSheet.create({
    lista:{
        flexDirection: 'column',
    },
    cabecalho:{
        borderRadius: 5,
        backgroundColor: "#e27109",
        marginHorizontal: 25, 
        marginVertical: 10,        
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    textoCabecalho:{
        textAlign: 'center',
        color: "#ffffff",
        margin:15,
        fontSize:25,
    },
    informacoes:{
        alignItems: 'center',
    },
    itemLista:{
        borderBottomWidth: 1,
        borderBottomColor: "#e27109",
        height: 60,
        marginHorizontal: 15, 
        marginVertical: 5,
        justifyContent: 'space-between',
        flexDirection: 'row',
        
    },
    numeral:{
        textAlign: 'center',
        paddingTop: 5,
        flex:1,     
        fontSize: 20,
    },
    nomeTime: {
        paddingTop: 5,
        textAlign: 'center',
        flex:1,
        fontSize: 20,
    },
    escudo:{
        width: 45,
        height: 45,
    }
});