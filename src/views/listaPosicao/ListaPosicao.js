import React, {Component} from 'react';
import { Image, Text, View, ScrollView, FlatList, TouchableOpacity, Alert } from 'react-native';
import CartolaFetchService from '../../services/CartolaFetchService';
import { STYLES } from './styles';

// posição, o escudo e o nome do clube.

export default class ListaPosicao extends Component{

    constructor(){
        super();
        this.state = {
            clubes: [],
            todosClubes: [],
            status: "",
            order: ""
        }
    }

    orderByPosicaoAsc = () => {
        //console.log('Acrescendo');
        this.state.todosClubes.sort(function(a, b) {      
            return a.posicao - b.posicao;
        });
        
        this.setState({clubes: this.state.todosClubes.slice(0,10)});
    }

    orderByPosicaoDesc = () => {
        //console.log('Decrescendo');
        this.state.todosClubes.sort(function(a, b) {      
            return b.posicao - a.posicao;
        });
        
        this.setState({clubes: this.state.todosClubes.slice(0,10)});
    }

    orderByPosicao = () => {
        if(this.state.order === "ʌ"){
            this.orderByPosicaoDesc();
            this.setState({order: "v"});
        }else{
            this.orderByPosicaoAsc();
            this.setState({order: "ʌ"});
        }        
    }

    getClube(id_clube){
        let clubeProcurado;
        this.state.clubes.forEach(clube => {
            if(clube.id == id_clube)
                clubeProcurado = clube
        });
        return clubeProcurado;
    }

    getEscudoClube(id_clube){
        let clube = this.getClube(id_clube);
        return clube.escudos["45x45"];
    }

    load = () => {
        CartolaFetchService.get()
            .then(json => {
                let arrayClubes = Object.values(json.clubes);
                this.setState({todosClubes: arrayClubes, status: 'NORMAL'});
                this.setState({clubes: arrayClubes.slice(0,10), status: 'NORMAL'});
                this.orderByPosicaoAsc();
                this.setState({order: "ʌ"});
            })
            .catch(e => 
                this.setState({status: 'FALHA_CARREGAMENTO'})
            );
    }

    componentWillMount(){
        // Apagar
        //this.getClube();
        //------
        this.load();
    }
    
    render(){  
        
        if(this.state.status === 'FALHA_CARREGAMENTO')
            return (
                <TouchableOpacity onPress={this.load}>
                    <Text>Ops...</Text>
                    <Text>Não foi possível carregar a Lista</Text>
                    <Text>Toque para tentar novamente</Text>
                </TouchableOpacity>
            );

        return (
            <ScrollView >
                <View style={STYLES.lista}>
                    <View style={STYLES.cabecalho}>
                        <TouchableOpacity onPress={this.orderByPosicao}>
                            <Text style={STYLES.textoCabecalho}>Posição {this.state.order}</Text>
                        </TouchableOpacity>
                        <Text style={STYLES.textoCabecalho}>Time</Text>
                    </View>
                    <FlatList 
                        data={this.state.clubes}                        
                        keyExtractor={item => String(item.id)}
                        renderItem={(item) => 
                            <View style={STYLES.itemLista}>                        
                                <Text style={STYLES.numeral} >{item.item.posicao}</Text>
                                <Image style={STYLES.escudo} 
                                    source={{uri: this.getEscudoClube(item.item.id)}}/>  
                                <Text style={STYLES.nomeTime} >{item.item.nome}</Text>                           
                            </View>
                        }                   
                    />
                </View>   

            </ScrollView>
        );
    }

}