import ListaPosicao from './ListaPosicao';
import { ListaPosicaoNavigationOption } from './ListaPosicaoNavigationOptions';

export const POSICAO = {
    screen: ListaPosicao,
    navigationOptions: ListaPosicaoNavigationOption,
};