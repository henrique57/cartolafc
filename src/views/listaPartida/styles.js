import { StyleSheet } from 'react-native';

export const STYLES = StyleSheet.create({
    titulo:{
        // flexDirection: "row",
        // color: "#ffffff",
        // marginHorizontal: 25, 
        // marginVertical: 10,
        borderRadius: 5,
        backgroundColor: "#e27109",
        marginHorizontal: 25, 
        marginVertical: 10,        
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    textoTitulo:{
        textAlign: 'center',
        color: "#ffffff",
        margin:15,
        fontSize:25,
    },
    lista:{
        alignItems: 'center',
        flexDirection: 'column',
    },
    cabecalho:{
        flexDirection: 'row',
    },
    informacoes:{
        alignItems: 'center',
    },
    itemLista:{
        alignItems: 'center',
        marginBottom: 20,
        backgroundColor: '#efbb7f',
        borderRadius: 20,
        height: 175,
        width: 300,
    },
    clube:{
        alignItems: 'center',
        flexDirection: 'column',
        margin: 20,    
    },
    escudo:{
        width: 60,
        height: 60
    },
    separadorClubes:{
        marginTop:50,
        fontSize: 25,
    }
});