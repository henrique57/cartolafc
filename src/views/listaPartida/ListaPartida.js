import React, {Component} from 'react';
import { Text, View, ScrollView, FlatList, TouchableOpacity, Image } from 'react-native';
import CartolaFetchService from '../../services/CartolaFetchService';
import { STYLES } from './styles';

export default class ListaPartida extends Component{

    constructor(){
        super();
        this.state = {
            data: [],
            status: ""
        }
    }
    sortPartidas(json){
        let partidasOrder = json.partidas;
        partidasOrder.sort(function(a, b) {
            return new Date(a.partida_data) - new Date(b.partida_data);
        });
        json.partidas = partidasOrder;
        return json;        
    }

    getClube(id_clube){
        return this.state.data.clubes[id_clube];
    }

    getEscudoClube(id_clube){
        return this.getClube(id_clube).escudos["60x60"];
    }

    getAbreviacaoClube(id_clube){
        return this.getClube(id_clube).abreviacao;
    }

    getDataCorreta = (date) =>{
        let data = new Date(date);
        let dataCorreta = 
            (data.getDay() > 10 ? data.getDay() : '0'+ data.getDay()) +'/'+
            (data.getMonth() > 10 ? data.getMonth() : '0'+ data.getMonth())+'/'+
            data.getFullYear()+' '+
            (data.getHours() > 10 ? data.getHours() : '0'+ data.getHours())+':'+
            (data.getMinutes() > 10 ? data.getMinutes() : '0'+ data.getMinutes());
        return dataCorreta;
    }

    load = () =>{
        CartolaFetchService.get()
          .then(json => {
                jsonOrder = this.sortPartidas(json);
                this.setState({data: jsonOrder, status: 'NORMAL'})
              //console.log( this.state.data.clubes)
            }
            ).catch(e => this.setState({status: 'FALHA_CARREGAMENTO'}));
    }

    componentWillMount(){
        this.load();
    }

    render(){  
        
        if(this.state.status === 'FALHA_CARREGAMENTO')
            return (
                <TouchableOpacity onPress={this.load}>
                    <Text>Ops...</Text>
                    <Text>Não foi possível carregar a Lista</Text>
                    <Text>Toque para tentar novamente</Text>
                </TouchableOpacity>
            );

        return (
            <ScrollView >

                <View style={STYLES.lista}>
                    <View style={STYLES.titulo}>
                        <Text style={STYLES.textoTitulo}>Próximas Partidas</Text>
                    </View>
                          
                    <FlatList 
                        data={this.state.data.partidas}
                        keyExtractor={item => String(item.partida_id)}
                        renderItem={({item}) => 
                            <View style={STYLES.itemLista}>                            
                                <View style={STYLES.cabecalho}>
                                    <View style={STYLES.clube}>
                                        <Image style={STYLES.escudo} 
                                            source={{uri: this.getEscudoClube(item.clube_casa_id)}}/>
                                        <Text>{this.getAbreviacaoClube(item.clube_casa_id)}</Text>
                                    </View>
                                    <Text style={STYLES.separadorClubes}>X</Text>
                                    <View style={STYLES.clube}>
                                        <Image style={STYLES.escudo} 
                                            source={{uri: this.getEscudoClube(item.clube_visitante_id)}}/>   
                                        <Text>{this.getAbreviacaoClube(item.clube_visitante_id)}</Text>
                                    </View>
                                </View> 

                                <View style={STYLES.informacoes}>
                                    <Text>{this.getDataCorreta(item.partida_data)}</Text>
                                    <Text>Local: {item.local} </Text>
                                </View>
                                
                            </View>
                        }                   
                    />
                </View>   

            </ScrollView>
        );
    }

}