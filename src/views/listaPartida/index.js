import ListaPartida from './ListaPartida';
import { ListaPartidaNavigationOption } from './ListaPartidaNavigationOptions';

export const PARTIDA = {
    screen: ListaPartida,
    navigationOptions: ListaPartidaNavigationOption,
};